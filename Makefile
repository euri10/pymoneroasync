.DEFAULT_GOAL := help

PY_SRC := pymoneroasync
PY_SRC_TEST := tests
PY_DOCS := docs

.PHONY: check
check: check-black check-flake8 check-isort check-mypy

.PHONY: check-black
check-black:  ## Check if code is formatted nicely using black.
	black -v --check --diff $(PY_SRC) $(PY_SRC_TEST) $(PY_DOCS)

.PHONY: check-flake8
check-flake8:  ## Check for general warnings in code using flake8.
	flake8 --exclude=.venv $(PY_SRC) $(PY_SRC_TEST) $(PY_DOCS)

.PHONY: check-mypy
check-mypy:  ## Check for general warnings in code using mypy.
	mypy $(PY_SRC)

.PHONY: check-isort
check-isort:  ## Check for general warnings in code using isort.
	isort --check --diff $(PY_SRC) $(PY_SRC_TEST)

.PHONY: clean
clean: ## Delete temporary files.
	@rm -rf build 2>/dev/null
	@rm -rf dist 2>/dev/null
	@rm -rf .coverage* 2>/dev/null
	@rm -rf .pytest_cache 2>/dev/null
	@rm -rf .mypy_cache 2>/dev/null
	@rm -rf pip-wheel-metadata 2>/dev/null

.PHONY: help
help:  ## Print this help.
	@awk 'BEGIN {FS = ":.*?## "} /^[a-zA-Z0-9_-]+:.*?## / {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}' $(MAKEFILE_LIST) | sort

.PHONY: lint-black
lint-black:  ## Lint the code using black.
	black $(PY_SRC) $(PY_SRC_TEST) $(PY_DOCS)

.PHONY: format
format:  ## Format everything ie isort and black
	black $(PY_SRC) $(PY_SRC_TEST) $(PY_DOCS)
	isort  $(PY_SRC) $(PY_SRC_TEST)

.ONESHELL:
.PHONY: setup
setup:  ## Setup the development environment.
	poetry install
	docker-compose -f ./tests/mymonero/docker-compose.yml up -d --build

.PHONY: test
test: clean  ## Run the tests using coverage with pytest module.
	coverage run

.PHONY: report
report: ## coverage report
	coverage report

.PHONY: html
html: ## coverage report
	coverage html

.PHONY: docs
docs:  ## build sphinx docs
	cd docs
	make linkcheck
	make html