![pipeline](https://gitlab.com/euri10/pymoneroasync/badges/master/pipeline.svg)
![coverage](https://gitlab.com/euri10/pymoneroasync/badges/master/coverage.svg)

**pymoneroasync** is an async python library for monero

it's in alpha state, API is likely to change without warning

[documentation](https://euri10.gitlab.io/pymoneroasync)
