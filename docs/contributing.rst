Contributing
============

pymoneroasync is a community-maintained project and we happily accept contributions.

If you wish to add a new feature or fix a bug:

#. `Check for open issues <https://gitlab.com/euri10/pymoneroasync/-/issues>`_ or open
   a fresh issue to start a discussion around a feature idea or a bug. There is
   a *Contributor Friendly* tag for issues that should be ideal for people who
   are not very familiar with the codebase yet.
#. Fork the `pymoneroasync repository on GitLab <https://gitlab.com/euri10/pymoneroasync/-/issues>`_
   to start making your changes.
#. Write a test which shows that the bug was fixed or that the feature works
   as expected.
#. Format your changes with black using command `make lint-black` and lint your
   changes using command `make format`.
#. Send a pull request and bug the maintainer until it gets merged and published.


Setting up your development environment
---------------------------------------

.. note:: TLDR install everything with:

    $ make setup


In order to setup the development environment all that you need is:

- Install the python dependencies for the project, ie poetry and run:

    $ poetry install

- Install a private testnet against which all the tests will run, for that you'll need `docker <https://docs.docker.com/get-docker/>`_  and `docker-compose <https://docs.docker.com/compose/install/>`_ installed in your machine. Once installed you can run a private monero testnet using:

    $ docker-compose -f tests/mymonero/docker-compose.tml up -d --build


Running the tests
-----------------

We use some external dependencies, multiple interpreters and code coverage
analysis while running test suite. Our `Makefile` handles much of this for
you:

   $ make test
