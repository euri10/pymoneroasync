.. pymoneroasync documentation master file, created by
   sphinx-quickstart on Thu Feb 11 13:46:19 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

pymoneroasync's documentation!
==============================

.. toctree::
   :hidden:
   :maxdepth: 3

   user-guide
   reference/index
   contributing

**pymoneroasync** is an async python library for `monero <https://www.getmonero.org/>`_.

It aims at being agnostic to the http library used (provided it is async of course).

Currently, only an `httpx <https://www.python-httpx.org/>`_ client has been implemented.

.. note::
   an `aiohttp <https://docs.aiohttp.org/en/stable/>`_ client will be implemented once they support DigestAuth.

**pymoneroasync** let you send all the RPC methods available in the `Monero daemon RPC <https://www.getmonero.org/resources/developer-guides/daemon-rpc.html>`_.

.. hint::
   As httpx is agnostic about the async library used you can either use asyncio, trio, curio, see the httpx documentation.

Nothing is available yet on the wallet side of things, this is the next goal.

Installing
----------

pymoneroasync can be installed with `pip <https://pip.pypa.io/en/stable/>`_

.. code-block:: bash

  $ python -m pip install pymoneroasync

Alternatively, you can grab the latest source code from `GitLab <https://gitlab.com/euri10/pymoneroasync>`_:

.. code-block:: bash

  $ git clone https://gitlab.com/euri10/pymoneroasync

.. code-block:: bash

  $ python setup.py install


Usage
-----

The :doc:`user-guide` is the place to go to learn how to use the library and
accomplish common tasks.

The :doc:`reference/index` documentation provides API-level documentation.


License
-------

TODO

Contributing
------------

We happily welcome contributions, please see :doc:`contributing` for details.


Tips
----

If you like the library you can send a tip to

.. code-block::

   45bwziwDSVZARYA7vgk1156ug8C2grGwLLgzeUNJR8aL8BaG5cHAFurCyVX147UUsh2B2vH4asnJ8dCEyA3RLRLR4TRGYBy
