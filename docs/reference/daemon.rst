Daemon
======

The methods of the AsyncDaemon all return a pydantic object from the :doc:`daemon.outputs`.

If the rpc method requires an input, it will be an object described in :doc:`daemon.inputs`.

.. autoclass:: pymoneroasync._abc.AsyncDaemon
    :members:
    :show-inheritance:
