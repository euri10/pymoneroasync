User guide
==========

If you want to play with the library without the "real" monero mainnet or testnet you can run the docker-compose file.

This creates a private testnet with 2 deterministic wallets.

We use this for testing but it can be used for discovering how to use the library.

.. asciinema:: run_private_testnet.cast

In the cast below you'll find an example of using AsyncDaemon with asyncio.

You can notice that you'll get autocompletion for all inputs, and methods.

You can also see that the response objects are pydantic objects with well defined types.

For instance the `PeerListEntry` object you end up with has a host property of type `IPv4Address` and its `last_seen` property is directly a datetime object.

.. asciinema:: usage.cast

The block below is a little sample of how to use the daemon with asyncio and httpx:

.. code-block:: python

    import asyncio
    import httpx
    from pymoneroasync import _httpx as monero_httpx_daemon


    async def main():
        async with httpx.AsyncClient() as client:
            d = monero_httpx_daemon.AsyncDaemon(
                client, username="daemonuser", password="daemonpass", port=28081
            )
            bc = await d.get_block_count()
            print(bc.count)
            c = await d.get_connections()
            print(c.connections)
            for con in c.connections:
                print(con.avg_upload)

    if __name__ == "__main__":
        asyncio.run(main())


And this one shows you can also use trio should you prefer:

.. code-block:: python

    import trio

    import httpx
    from pymoneroasync import _httpx as monero_httpx_daemon


    async def main():
        async with httpx.AsyncClient() as client:
            d = monero_httpx_daemon.AsyncDaemon(
                client, username="daemonuser", password="daemonpass", port=28081
            )
            r = await d.get_block_count()
            print(r.count)


    if __name__ == "__main__":
        trio.run(main)