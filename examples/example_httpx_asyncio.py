import asyncio
import httpx
from pymoneroasync import httpx as monero_daemon_httpx


async def main():
    async with httpx.AsyncClient() as client:
        d = monero_daemon_httpx.AsyncDaemon(
            client, username="daemonuser", password="daemonpass", port=28081
        )
        bc = await d.get_block_count()
        print(bc.count)
        c = await d.get_connections()
        print(c.connections)
        for con in c.connections:
            print(con.avg_upload)

if __name__ == "__main__":
    asyncio.run(main())
