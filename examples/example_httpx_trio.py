import trio

import httpx
from pymoneroasync import httpx as monero_daemon_httpx


async def main():
    async with httpx.AsyncClient() as client:
        d = monero_daemon_httpx.AsyncDaemon(
            client, username="daemonuser", password="daemonpass", port=28081
        )
        r = await d.get_block_count()
        print(r.count)


if __name__ == "__main__":
    trio.run(main)
