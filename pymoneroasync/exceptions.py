class Unauthorized(Exception):
    pass


class LoadingAccountsError(Exception):
    pass


class Unhandled(Exception):
    pass


class DaemonParsingError(Exception):
    pass
