from pathlib import Path

import httpx
import pytest

from pymoneroasync.const import W_TESTNET_RPC_PORT

WALLET1 = "9vUnTucAioDHD4ZqrFHXAgfLqrsC3LkZ6JFr5axBLhDiFMaHuEk33aqXimoZEMtQh5ibdYxcNSBw2hBZLAsCnuw4B4rBeZX"  # noqa: E501


def is_responsive(url):
    try:
        response = httpx.get(url)
        if response.status_code == 401:
            return True
    except Exception as e:
        print(e)
        return False


@pytest.fixture(scope="session")
def docker_compose_project_name(pytestconfig):
    return "monerodev"


@pytest.fixture(scope="session")
def docker_compose_file(pytestconfig):
    dc_path = Path(pytestconfig.rootdir) / "tests" / "mymonero" / "docker-compose.yml"
    return dc_path.as_posix()


@pytest.fixture(scope="session")
def private_monero_testnet(docker_ip, docker_services):
    """
    Yield the testnet container
    """
    port = docker_services.port_for("monerodev", W_TESTNET_RPC_PORT)
    host = "monerodev"
    url = "http://{}:{}".format(host, port)
    print(url)
    docker_services.wait_until_responsive(
        timeout=30.0, pause=0.1, check=lambda: is_responsive(url)
    )
    yield
