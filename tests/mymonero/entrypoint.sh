#!/bin/sh
#set -ex

monerod --$NETTYPE --no-igd --hide-my-port --data-dir /$NETTYPE/node_01 --p2p-bind-ip 127.0.0.1 --log-level 1 --rpc-bind-ip 0.0.0.0 --add-exclusive-node 127.0.0.1:48080 --confirm-external-bind --detach --fixed-difficulty $DIFFICULT --p2p-bind-port 28080 --rpc-bind-port 28081 --zmq-rpc-bind-port 28082 --rpc-login $D_RPC_USER:$D_RPC_PASS

# ports for this node are not 18 (mainnet) nor 28 (testnet) nor 38 (stagenet)
monerod --$NETTYPE --no-igd --hide-my-port --data-dir /$NETTYPE/node_02 --p2p-bind-ip 127.0.0.1 --log-level 1 --rpc-bind-ip 0.0.0.0 --add-exclusive-node 127.0.0.1:28080 --confirm-external-bind --detach --fixed-difficulty $DIFFICULT --p2p-bind-port 48080 --rpc-bind-port 48081 --zmq-rpc-bind-port 48082 --rpc-login $D_RPC_USER:$D_RPC_PASS

echo "GENERATE WALLET1"
echo "exit" | monero-wallet-cli --$NETTYPE --generate-new-wallet /$NETTYPE/wallet_01.bin  --restore-deterministic-wallet --electrum-seed="sequence atlas unveil summon pebbles tuesday beer rudely snake rockets different fuselage woven tagged bested dented vegan hover rapid fawns obvious muppet randomly seasons randomly" --password "wallet1pass" --log-file /$NETTYPE/wallet_01.log --daemon-login $D_RPC_USER:$D_RPC_PASS
echo "0"

echo "GENERATE WALLET2"
echo "exit" | monero-wallet-cli --$NETTYPE --generate-new-wallet /$NETTYPE/wallet_02.bin  --restore-deterministic-wallet --electrum-seed="deftly large tirade gumball android leech sidekick opened iguana voice gels focus poaching itches network espionage much jailed vaults winter oatmeal eleven science siren winter" --password "wallet2pass" --log-file /$NETTYPE/wallet_02.log --daemon-login $D_RPC_USER:$D_RPC_PASS
echo "0"

monero-wallet-rpc --$NETTYPE --trusted-daemon --wallet-file /$NETTYPE/wallet_01.bin --password "wallet1pass" --log-file /$NETTYPE/wallet_01.log --rpc-bind-port 29081 --rpc-login $W_RPC_USER:$W_RPC_PASS --rpc-bind-ip 0.0.0.0 --daemon-login $D_RPC_USER:$D_RPC_PASS --confirm-external-bind --detach
monero-wallet-rpc --$NETTYPE --trusted-daemon --wallet-file /$NETTYPE/wallet_02.bin --password "wallet2pass" --log-file /$NETTYPE/wallet_02.log --rpc-bind-port 29082 --rpc-login $W_RPC_USER:$W_RPC_PASS --rpc-bind-ip 0.0.0.0 --daemon-login $D_RPC_USER:$D_RPC_PASS --confirm-external-bind --detach

exec "$@"
