FROM debian:buster-20210111-slim
ARG monero_version=v0.17.1.9
RUN echo $monero_version
RUN DEBIAN_FRONTEND=noninteractive && \
        apt-get update && \
        apt-get install -y wget tar bzip2 && \
        apt-get -y clean && \
        rm -rf /var/lib/apt/lists/*
RUN wget https://downloads.getmonero.org/cli/monero-linux-x64-$monero_version.tar.bz2
RUN tar jxvf monero-linux-x64-$monero_version.tar.bz2
RUN cd monero-x86_64-linux-gnu-$monero_version
RUN ls -la
ENV PATH="/monero-x86_64-linux-gnu-$monero_version:${PATH}"
WORKDIR testnet
ENV NETTYPE=testnet
ENV DIFFICULT=1
ENV W_RPC_USER=wrpcuser
ENV W_RPC_PASS=wrpcpass
ENV D_RPC_USER=daemonuser
ENV D_RPC_PASS=daemonpass

EXPOSE 29081
COPY entrypoint.sh /entrypoint.sh
ENTRYPOINT ["/entrypoint.sh"]

CMD ["bash", "-c", "while true; do sleep 1; done"] 

