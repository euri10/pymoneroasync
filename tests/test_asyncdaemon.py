import logging
import os

import httpx
import pytest

from pymoneroasync import httpx as monero_daemon_httpx
from pymoneroasync.const import (
    DAEMON_DEFAULT_LOG,
    DAEMON_DEFAULT_LOG_RPC,
    DAEMON_JSON_RPC_METHODS_INPUTS,
    DAEMON_JSON_RPC_METHODS_OUTPUTS,
    DAEMON_OTHER_RPC_METHODS_INPUTS,
    DAEMON_OTHER_RPC_METHODS_OUTPUTS,
    TESTNET_RPC_PORT,
)
from tests.conftest import WALLET1

logger = logging.getLogger(__name__)


@pytest.fixture
async def mhttpx_daemon():
    async with httpx.AsyncClient() as client:
        d = monero_daemon_httpx.AsyncDaemon(
            client,
            username="daemonuser",
            password="daemonpass",
            host=os.environ.get("MONERODEV", "127.0.0.1"),
            port=TESTNET_RPC_PORT,
        )
        yield d


DAEMON_JSON_RPC_METHODS_TEST = [
    ("get_block_count", None),
    ("on_get_block_hash", [0]),
    (
        "get_block_template",
        {
            "wallet_address": WALLET1,
            "reserve_size": 0,
        },
    ),
    (
        "submit_block",
        [
            "010ef483e6800648ca7cd3c8de5b6a4d53d2861fbdaedca141553559f9be9520068053cda8430b00000000013d01ff010680a0db50029e38b2e971b7df9f764bab4688bf4a1e722ef0422d06792fa3dc5d04a9d9123880a8d6b90702540a63c21b4581baa6623bf2c28fa7b14f0894d1c41bbf7cef4382ef40cbdd178088aca3cf0202afef316b95a30671961b494b2f89822d000ec1ea64094a4a1181ee6c5f70d6728090cad2c60e02b9410fce0d6adfc1203c332b978342acbc2fcd55df98db06f241dfb3d1a1904e80e08d84ddcb010246ef645101d36af19b6a5fa3bf542729fa257071f97c77737a7c531a273a973780c0caf384a3020225de6f1e791c11a712416d0c51602f7add8a2406a01e95b4a246b4575e2c45e95f01631e651e2e50d9a86b713dafb48cd92a48ee106f7cc17a322a79bf5596b96a6c023c00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000"  # noqa: E501
        ],
    ),
    ("get_last_block_header", {"fill_pow_hash": True}),
    ("get_last_block_header", {"fill_pow_hash": False}),
    ("get_block_header_by_hash", {"hash": ""}),
    (
        "get_block_header_by_hash",
        {"hash": "48ca7cd3c8de5b6a4d53d2861fbdaedca141553559f9be9520068053cda8430b"},
    ),
    (
        "get_block_header_by_hash",
        {
            "hashes": [
                "48ca7cd3c8de5b6a4d53d2861fbdaedca141553559f9be9520068053cda8430b"
            ]
        },
    ),
    ("get_block_header_by_height", {"height": 1}),
    ("get_block_headers_range", {"start_height": 0, "end_height": 1}),
    (
        "get_block",
        {"hash": "48ca7cd3c8de5b6a4d53d2861fbdaedca141553559f9be9520068053cda8430b"},
    ),
    ("get_block", {"height": 1}),
    ("get_connections", None),
    ("get_info", None),
    ("hard_fork_info", None),
    (
        "set_bans",
        {
            "bans": [
                {"host": "192.168.1.100", "ban": True, "seconds": 30},
            ]
        },
    ),
    (
        "set_bans",
        {
            "bans": [
                {"ip": "3232235829", "ban": True, "seconds": 30},
            ]
        },
    ),
    (
        "set_bans",
        {
            "bans": [
                {"host": "192.168.1.100", "ban": False, "seconds": 30},
            ]
        },
    ),
    ("get_bans", None),
    (
        "flush_txpool",
        {"txids": []},
    ),
    ("get_output_histogram", {"amounts": [2]}),
    ("get_coinbase_tx_sum", {"height": 1, "count": 2}),
    ("get_version", None),
    ("get_fee_estimate", {}),
    ("get_fee_estimate", {"grace_blocks": 1}),
    ("get_alternate_chains", None),
    # TODO: fix txids
    ("relay_tx", {"txids": []}),
    ("sync_info", None),
    ("get_txpool_backlog", None),
    (
        "get_output_distribution",
        {"amounts": [628780000], "from_height": 1, "binary": False},
    ),
]


jsonrpc_methods_params = [
    (k, DAEMON_JSON_RPC_METHODS_INPUTS[k], DAEMON_JSON_RPC_METHODS_OUTPUTS[k], p)
    for (k, p) in DAEMON_JSON_RPC_METHODS_TEST
]
jsonrpc_methods_params_ids = [
    f"{j}-{e.__name__}-{str(p)}" for (j, i, e, p) in jsonrpc_methods_params
]


@pytest.mark.asyncio
@pytest.mark.parametrize(
    "jsonrpc_method, objIn, objOut, params",
    jsonrpc_methods_params,
    ids=jsonrpc_methods_params_ids,
)
async def test_daemon_jsonrpc(mhttpx_daemon, jsonrpc_method, objIn, objOut, params):
    funcname = getattr(mhttpx_daemon, jsonrpc_method)
    if params is not None:
        if isinstance(params, dict):
            paramsObj = objIn(**params)
        else:
            paramsObj = objIn(__root__=params)
        response = await funcname(paramsObj)
    else:
        response = await funcname()

    logger.info(response)
    assert isinstance(response, objOut)


DAEMON_OTHER_RPC_METHODS_TEST = [
    ("get_height", None),
    # ("get_blocks_bin",
    # {"block_ids": [1,2,3,4,5,6,7,8,9,10], "start_height": 1, "prune": False}),
    # ("getblocks.bin", {"block_ids": [1], "start_height": 0, "prune": False}),
    # ("get_blocks_by_height.bin", {"heights":[ 0,1 ]}),
    # ("getblocks_by_height.bin", {"heights": [0, 1]}),
    # ("get_hashes.bin", None),
    # ("gethashes.bin", None),
    # ("get_o_indexes.bin", {"txid": "dswdw"}),
    # ("get_outs.bin", None),
    (
        "get_transactions",
        {
            "txs_hashes": [
                "0999f9e96bee361274a130b8f57db97c33afa0effb1193b9d6f65749813b9ca2"
            ],
            "decode_as_json": True,
            "prune": True,
        },
    ),
    ("get_alt_blocks_hashes", None),
    (
        "is_key_image_spent",
        {
            "key_images": [
                # TODO: find key image to get a return
                "8d1bd8181bf7d857bdb281e0153d84cd55a3fcaa57c3e570f4a49f935850b5e3",
            ]
        },
    ),
    (
        "send_raw_transaction",
        {
            "tx_as_hex": "0999f9e96bee361274a130b8f57db97c33afa0effb1193b9d6f65749813b9ca2",  # noqa: E501
            "do_not_relay": False,
        },
    ),
    (
        "start_mining",
        {
            "do_background_mining": False,
            "ignore_battery": True,
            "miner_address": WALLET1,
            "threads_count": 1,
        },
    ),
    ("stop_mining", None),
    ("mining_status", None),
    ("save_bc", None),
    ("get_peer_list", None),
    ("set_log_hash_rate", {"visible": True}),
    ("set_log_level", {"level": 0}),
    ("set_log_categories", {"categories": DAEMON_DEFAULT_LOG}),
    ("set_log_categories", {"categories": DAEMON_DEFAULT_LOG_RPC}),
    ("get_transaction_pool", None),  # TODO: get a real info too
    # # ("get_transaction_pool_hashes.bin", None),
    ("get_transaction_pool_stats", None),
    ("set_limit", {"limit_down": 128, "limit_up": 256}),
    ("get_limit", None),
    ("out_peers", {"out_peers": 10}),
    ("in_peers", {"in_peers": 10}),
    # ("start_save_graph", None), # commented as obsolete
    # ("stop_save_graph", None), # commented as obsolete
    ("get_outs", {"outputs": [], "get_txid": True}),
    ("update", {"command": "check"})
    # ("stop_daemon", None),
]


other_rpc_methods_params = [
    (k, DAEMON_OTHER_RPC_METHODS_INPUTS[k], DAEMON_OTHER_RPC_METHODS_OUTPUTS[k], p)
    for (k, p) in DAEMON_OTHER_RPC_METHODS_TEST
]
other_rpc_methods_params_ids = [
    f"{j}-{e.__name__}-{str(p)}" for (j, i, e, p) in other_rpc_methods_params
]


@pytest.mark.asyncio
@pytest.mark.parametrize(
    "other_rpc_method, objIn, objOut, params",
    other_rpc_methods_params,
    ids=other_rpc_methods_params_ids,
)
async def test_daemon_other_rpc(mhttpx_daemon, other_rpc_method, objIn, objOut, params):
    funcname = getattr(mhttpx_daemon, other_rpc_method)
    if params is not None:
        if isinstance(params, dict):
            paramsObj = objIn(**params)
        else:
            paramsObj = objIn(__root__=params)
        response = await funcname(paramsObj)
    else:
        response = await funcname()

    logger.info(response)
    assert isinstance(response, objOut)
